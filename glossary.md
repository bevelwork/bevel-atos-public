# Glossary

[[_TOC_]]

## A

1. ACO - Appropriation Control Officer
1. ADC - Austin Data Center
1. API - Application Programming Interface
1. APM - Application Portfolio Management
1. Asset Inventory and Management System (AIMS) - An automated, database‐driven application used to store, query, and maintain asset inventory information for all assets used in association with the Services, whether the assets are located at DIR Facilities or Successful Respondent Facilities. The AIMS provides an inventory of the IT infrastructure managed by the Successful Respondent.
1. ASU - Angelo State University
1. Availability or Available - The full functionality of a Service Component is ready and accessible for use by the Authorized Users and is not degraded in any material respect.

## B

1. BELC - Business Executive Leadership Committee
1. Business Day - Each day from Monday through Friday, excluding State holidays, 7:00 a.m. to 5:00 p.m., Local Time. State holidays will include all holidays with the status “All agencies closed.” State holidays will not include State optional holidays or holidays that require skeleton crews. For SLAs related to outbound mail Services, Business Day means each day from Monday through Friday, excluding US postal holidays, 7:00 a.m. to 5:00 p.m., Local Time. For SLA reporting purposes, the hours listed in Attachment 1.3 Service Level Definitions and Performance Analytics would override the 7:00 a.m. to 5:00 p.m.

## C

1. CAB - Change Advisory Board
1. CASB - Cloud Access Security Broker
1. CCP - Common Checkout Page
1. CDC - Consolidated Data Center
1. CFSG - Contracts and Finance Solution Group
1. CI - Configuration Item. A CI represents a single resource, server, or appliance maintained in theChange Management Database (CMDB)
1. CMDB - Configuration Management Database
1. CMP - Cloud Management Platform
1. COTS - Commercial Off-The-Shelf Software - Equipment and/or Software, as applicable, that is readily available to the public from a Third Party that is not an Affiliate of a Party.
1. CR - Customer Representative
1. CRQ - Change Request
1. CSI - Continuous Service Improvement
1. CSI - Center for Service Integration
1. CSM - Customer Service Manager

## D

1. DB - Database
1. DBaaS - Database as a Service
1. DCS - Data Center Services
1. DCS Customer - Collectively, any of the following Entities that are designated by DIR to receive Services under the Agreement, whether directly from any DCS Service Component Provider or from DIR through an Interagency, Interlocal, or other agreement: (a) DIR in its capacity as a recipient of Services; (b) any State agency, unit of local government or institution of higher education as defined in Section 2054.003, Texas Government Code, and those State agencies that execute Interagency Agreements with DIR, as authorized by Chapter 771, Texas Government Code; (c) any Texas local government as authorized through the Interlocal Cooperation Act, Chapter 791, Texas Government Code; (d) any other state or governmental Entity of another state, as authorized by Section 2054.0565, Texas Government Code; (e) any other Entity permitted under Law to purchase Services from or through DIR; and (f) other Entities to which the Parties agree. The Parties acknowledge and agree that the definition of eligible DCS Customers is subject to modification by the State Legislature, and that the then‐current definition of DCS Customers shall control for all purposes
1. Dedicated Customer Appliance(RU) - A monthly recurring Charge to provide data center services for a physical or virtual appliance instance in a Consolidated Data Center (See MSA s4.1.4.4). See also `Physical Appliance Installation`.
1. Dedicated Server Installation Fee(RU) - One-time, non-recurring Charge for installation services for new Servers associated with the STM and dedicated to a single DCS Customer. The Server Installation Fee is only applicable for new Server installations resulting from a specific customer request and not applicable to general customer growth or Servers installed because of technology refresh, transition, or transformation.
1. DIR - Department of Information Resources
1. Disaster - (1) A sudden, unplanned calamitous event causing great damage or loss; (2) any event that creates an inability on an organizations part to provide critical business functions for some predetermined period of time; (3) in the business environment, any event that creates an inability on an organization’s part to provide the critical business functions for some predetermined period of time; (4) the period when company management decides to divert from normal production responses (in total or in part) and exercises its disaster recovery plan; and (5) typically signifies the beginning of a move from a primary to an alternate location
1. DR - Disaster Recovery
1. DRaaS - Disaster Recovery as a Service
1. DRE - Disaster Recovery Exercise

## E

1. EoSL - End of Service Life
1. ERM - Enterprise Relationship Manager
1. ESL - Expected Service Level
1. ETL - Extract, Transform, and Load. Used in the context of application data, databases, and big data.
1. Expense Line - The Expense line is a project component that takes elements from the quote approved by the agency and submits it for the next agency invoice.

## F

1. FY - Fiscal Year

## G

1. GHEP - Government Higher Education Program
1. GRC - Governance, Risk, and Compliance

## H

1. HIPAA - Health Insurance Portability and Accountability Act
1. HLA\ROM - High Level Architecture\Rough Order of Magnitude
1. HSC - Hardware Service Charge. One-time, non-recurring Charge for HSC Dedicated RUs compensate for hardware purchases of new Servers for DCS Customers’ dedicated use at the DCS Customer’s request, where support is charged by the Service Tier Matrix Server Support RU. **Note:** the term `HSC` can be used to refer to virtual or physical as well as appliance and server. It is best to avoid this term when possible and use a more specific term.
1. 'HSC - Dedicated Server' - A dedicated server for use solely by a single agency. This would be in contrast to the normal virtualized core and memory billing of the vblock (See also `Dedicated Server Installation Fee`).
1. 'HSC - Dedicated Hardware and Install CET' - Cost Estimation Tool (CET) for Hardware Service charges, associated installation and tax calculations.
1. 'HSC - Dedicated Quote' - Vendor-provided quote for Hardware Services. This is provided with 60 Months of software maintenance, pretax, and pre program costs.
1. HSP - HUB Subcontracting Plan

## I

1. IaaS - Infrastructure as a Service
1. IAC - Interagency Contract
1. IAM - Identity and Access Managemet
1. ILC - Interlocal Contract
1. Incident -An event which is not part of the standard operation of a Service and which causes or may cause disruption to or a reduction in the quality of Services and DIR and/or DCS Customer productivity.
1. ITBM - Information Technology Business Management
1. ITFM - Information Technology Financial Management
1. ITIL - A world‐wide recognized best‐practice framework for the management and delivery of IT services throughout their full lifecycle. The primary structure of the requirements in the Statements of Work are based on an ITIL v2 Foundations with ITIL v3 guidance in select functional areas (e.g., Request Management and Fulfillment) with the expectation of migrating towards ITIL v3 progressively as process improvements are incorporated into the SMM.
1. ITLC - Information Technology Leadership Committee
1. ITSCM - Information Technology Service Continuity Management
1. ITSLM - Information Technology Service Level Management
1. ITSM - Information Technology Service Management (Dcs ticketing system, the system of record)

## L

1. LDPS - Life, Death, Public Safety applications
1. LSE - Limited Support Environment

## M

1. MAL - Milestone Acceptance Letter
1. MAS - Managed Application Services
1. MIM - Major Incident Management\Manager
1. MIRT - Major Incident Response Team
1. MSI - Multi-sourcing Services Integrator - The SCP who has entered into a contract with DIR for Multi‐sourcing Services Integrator
   services.
1. MSL - Minimum Service Level
1. MSS - Managed Security Services
1. MTBF - Meantime between failures
1. MTTR - Meantime To Resolution

## N

1. NIST - National Institute of Standards and Technology.
1. Non-consolidated Compute - Includes service locations outside of the DIR CDCs as well as remote sites where break‐fix services will also be performed. Approximately twenty percent (20%) of the current server compute resides in agency‐owned legacy data centers or in remote agency business offices. Steady state operating requirements for these non‐consolidated locations are included in this Exhibit.

## O

1. O365 - Microsoft Office 365
1. ODA - On Demand Assessment
1. ODPUG - Open Data Portal User Group
1. OEM - Original Equipment Manufacturer
1. OA - Operating Agreement
1. OI - Operational Intelligence
1. OS - Operating System
1. OTC - One Time Charge.

## P

1. PaaS - Platform as a Service
1. Pass-Through Expenses - The expenses (identified in SOW Exhibit 2 Pricing, Section 3.7) which DIR has agreed to pay directly or reimburse on an Out‐of‐Pocket Expenses basis.
1. PAL - Project Acceptance Letter
1. PCM - Public Cloud Manager
1. PCI DSS - Payhment Card Industry Data Security Standards.
1. PCR - Project Change Request
1. PHI - Personal Health Information
1. Physical Appliance Installation(RU) - One-time, non-recurring Charge for installation services for new Physical Appliances (See MSA s4.1.4.5 for more details). **Note:** Physical Appliance Installation fees apply to refresh and normal growth inititives inititated by the customer. In this manner it is different than the `Dedicated Server Installation` RU.
1. PII - Personally Identifiable Information
1. PMD - Print, Mail, and Digitization
1. PMO - Portfolio Management Office
1. PMR - Problem Management Report
1. POE - Proof Of Entitlement
1. PPT - Personal Property Tax
1. PRB - Problem (usually in reference to the ticket type)

## Q

1. QSG - Quick Start Guide

## R

1. RAID - Risk, Action, Issue, Decision
1. RCA - Root Cause Analysis
1. RFS - Remote File Service
1. RFS - Request For Solution - A request for information, advice, access, or standard change to an IT service that does not require solution proposal development. Examples of such Service Request include provisioning ID access, password resets, and Service Catalog requests.
1. RITM - Requested Item
1. RPA - Robotic Process Automation
1. RPO - Recovery Point Objective. What is the acceptable amount of data loss.
1. RTO - Recovery Time Objective. What is the acceptable amount of system downtime.
1. RU - Resource Unit.A measurable device, unit of consumption, or other unit or resource utilization associated with the Services (as described in SOW Exhibit 2 Pricing) that is used for purposes of calculating Charges.

## S

1. SaaS - Software as a Service
1. SACM - Service Asset Configuration Management
1. SCP - Service Component Provider
1. SDC - San Angelo Data Center
1. SDM - Service Delivery Manager
1. SDSG - Service Delivery Solution Group
1. Server - Any computer that provides shared processing or resources (e.g., Application processing, database, mail, proxy, firewalls, backup capabilities, print, and fax services) to Authorized Users or other computers over the Network. A Server includes associated peripherals (e.g., local storage devices, attachments to centralized storage, monitor, keyboard, pointing device, tape drives, and external disk arrays) and is identified by a unique manufacturer's serial number.
1. Service Desk - The facilities, associated technologies, and fully trained staff who respond to Calls,facilitate all Incident Management, Problem Management, Change and RequestManagement activities, and act as a single point of contact for coordination andcommunication to Authorized Users and SCPs in regard to the Services.
1. SLA - Service Level Agreement
1. SME - Subject Matter Expert
1. SMM - Service Management Manual
1. SP&R - Service Performance and Reporting
1. SPECTRIM - Statewide Portal for Enterprise Cybersecurity Threat, Risk and Incident Management
1. SSC - Software Service Charge. One-time, non-recurring Charge for SSC Charges shall compensate for the Software procured on DIR or a DCS Customer’s behalf as provided in this provision and must follow the Procurement procedures specified in Exhibit 1 SOW, Section 3.6.1 Software and Hardware Purchases on behalf of Customer and SMM. The Software Services Charge is a methodology for DCS Customers to obtain Software licenses, maintenance and installation/configuration services, as applicable, for their dedicated use on STM Servers utilized by Successful Respondent in the provision of the Services. The types of Software which DIR and DCS Customer’s may obtain pursuant to this methodology are in the Attachment 2.2 Financial Responsibility Matrix. Software Service Charges shall be determined based on the Software Expenditures that are actually paid or incurred by Successful Respondent and shall be purchased in accordance with the Procurement Procedures specified in the SOW and SMM. SSC must be approved in advance by DIR or DCS Customer.
1. SRT - Schedules, Retentions, and Targets
1. STS - Shared Technology Services
1. SWI Statewide Intake (HHS application)

## T

1. TAC - Texas Administrative Code
1. TCF - Texas Cybersecurity Framework Assessment
1. Tech SG - Technology Solution Group
1. TPC - Texas Private Cloud
1. TPE - Transaction Processing Engine (Texas NIC Payment Engine)
1. TRG - Technical Recovery Guide
1. TSS - Technology Solution Services

## U

1. USAS - Uniform Statewide Accounting System

## V

1. VA - Virtual Assistant
1. VESDA Very Early Smoke Detection Apparatus.
1. VLANs Virtual Local Area Networks.
1. VM Virtual Machine.
1. VMDK Virtual Machine Disk.
1. VOC Volatile Organic Compound.
1. VOIP Voice Over Internet Protocol.
1. VPN Virtual Private Network.
1. VRF - Virtual Routing and forwarding (technology included in network routers)

## W

1. WAN - Wide Area Network - A long‐haul, high‐speed backbone transmission Network, consisting of WAN Equipment, Software, Transport Systems, Interconnect Devices, and Cabling that, and other services as they become available that are used to create, connect, and transmit data, voice and video signals, between or among: (i) LANs, and (ii) other locations that do business with the State and for which DIR is responsible for allowing Connectivity.
1. Wiring - Wiring that is generally permanent and embedded in the facility. Choices in cost and
   implementation are often driven by standards for the facility (BICSI or ANSI/TIA or other low‐voltage standards specifying such things as plenum or non‐plenum, UTP, Cat‐6e, etc.). Wiring installation often calls for certifications. Wiring installation often requires physical changes in the building (e.g., boring through walls or flooring) to be done in coordination with the building management.

1. Work Product -
   1. All reports and manuals, including transition plans, business requirements documents, design documents, manuals, training and knowledge transfer materials and documentation,
   1. the Service Management Manual,
   1. Desktop Procedures, and
   1. any intellectual property created as a result of this Agreement to express, embody or execute or perform a function, method or process that is specific to the business of DIR or DCS Customers.
1. WI - Work Instruction
