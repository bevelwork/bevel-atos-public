# Service Now Expense Line Records

[[_TOC_]]

## Summary

`Service Now Expense line records`(Expense Lines) serve as the means to automatically add HSC and a few other MISC charges onto the next appropriate bill. The process below walks through adding expense lines to an active project. All items being added to an expense line should be approved by the agency and be connect to a `current` quote. _NOTE:_ Be sure to check the expiration date.

This is how it will present to the agency on their next invoice:

![image-20201005151543069](../media/image-20201005151543069.png)

Here's how the details will show the Configuration Item (CI) information tied to that Hardware Service Charge - Dedicated Server (HSC - Dedicated Server) charge.

![image-20201005151641162](../media/image-20201005151641162.png)

## Process

### 1. Navigate to the Project

Project > Expense Lines > click "new"

![image-20201005152614423](../media/image-20201005152614423.png)

### 2. Fill in new Expense Line

Fill in as follows

| Name               | Value                                                                                              | Note                                                            |
| ------------------ | -------------------------------------------------------------------------------------------------- | --------------------------------------------------------------- |
| Configuration item | If cost is associated with a CI, it will need to be selected there to show up in the CMDB details. | `Note: This needs to be done first or may clear out your work!` |

![image-20201005154551270](../media/image-20201005154551270.png)

| Name         | Value                                                                                              |
| ------------ | -------------------------------------------------------------------------------------------------- |
| Source table | Hardware if there's a CI, or `Project table` for non-ci specific charges (e.g. Networking cables). |

![image-20201005152933284](../media/image-20201005152933284.png)

| Name         | Value                                                                                                                                                                                                                                                                                                                        |
| ------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Source ID    | Used the CI for the equipment if available. If using the project, use the service now project id.                                                                                                                                                                                                                            |
| Related cost | Total cost with tax from the `HSC - Dedicated and Installation CET`. Note that installation costs should be a separate expense line from the hardware and maintenance itself. This is done because there is a separate installation RUs (e.g. `Physical Appliance Installation RU` and `Dedicated Server Installation Fee`). |
| Process Date | Anticipated QA date (when the server is installed e.g. a green light is on for the server)                                                                                                                                                                                                                                   |
| Inherited    | Blank                                                                                                                                                                                                                                                                                                                        |
| State        | Pending until the QA date, will need to be updated by hand after QA date.                                                                                                                                                                                                                                                    |

### 3. Relate the HSC to the Expense Line

It's then really important to `relate` the `HSC Service Now Record ID` created by the the solution architect to the expense line. This will bring over the details of the purchase, or validate that the cost has been approved by the agency.

| Decscription | Details                                                                                                          |
| ------------ | ---------------------------------------------------------------------------------------------------------------- |
| RU           | HSC - Dedicated                                                                                                  |
| Description  | Simplify default content. Add "Expense line for invoicing `Service Now HSC id`," to the beginning of description |

![image-20201005153837730](../media/image-20201005153837730.png)

The `HSC Service Now Record` includes additional details about the equipment itself.

| Name          | Value         |
| ------------- | ------------- |
| Cost Type     | Purchase Cost |
| Cost Category | -- None --    |

![image-20201005154926492](../media/image-20201005154926492.png)

## Appendices

### Appendix A: Looking up the cost for a given 'HSC - Dedicated Servers/Appliances' and related 'Expense Line Items'.

#### Summary

All `HSC - Dedicated Server/Appliance` costs should be associated with a `Service Now HSC record` that was created as part of the solutioning process. This `Service Now HSC record` will include a total cost that will be used for the `Service Now Expense Line`.

By navigating to the `Service Now HSC record` and scrolling to the bottom, you will see the total of all quoted line items at the bottom like so:

![](../media/expense_line_1.png)

If additional information is needed you can review both the direct quote from the vendor which will contain:

1. Pre-tax totals for hardware and resources.
2. Any third party-provided installation costs that are passed directly to the customer. (Note if this is the case the `Phsyical Server Installation Fee` and the `Dedicated Appliance Installation Fee` are not applicable.)
3. Information about the equipment being ordered.

It is also **very important** to review the `HSC Dedicated Server and Installation CET` itself and copied to the `Service Now Project` (sometime multiple if there have been `PCR`s with version numbers.) as this will show:

1. The post-tax amounts that should correlate to the total you see on the `hsc`.
2. The quote expiration dates for all line items. This is way less tedious then looking for them within each of the quotes themselves.

Here's how the CET will look and where to pull the totals from. On the left-side you have the user-entered content:

![](../media/expense_line_2.png)

On the right-side you will have the tax, installation and totals. Use the total in the totals available in the `HSC – Dedicated Expense Line Cost`, `Installation Expense Line Cost`, or `Total Cost` as needed for your specific case.

![](../media/expense_line_3.png)

If the quote has expired prior to the agency approving the purchase, it can be renewed with the vendor and procurement. If the cost has changed since the original quote that was approved by the agency, a `PCR` will be required and the original architect should be engaged for rebuilding of the `CET`.

#### In the original CET

In the original CET the cost can be taken from column **'AZ'** which will include the quote, installation fees, and taxes associated with the asset as seen here:

![](../media/cet-total.png)

Note that each row should correlate to a different Expense Item.
