# HSC

## Overview

The hsc process has a few new responsibilities that we're looking to overview here. This will allow the automatic addition of HSC dedicated costs to the customers invoice in a predictable and automated manner.

The [Overall Process Doc](https://docs.google.com\spreadsheets\d\1Bg-kFgHOnX5GUrONk5qiS6OxVx2JuRCllGmIVmnsyZM\edit#gid=0) breaks this into a task level `RACI` with owners and timeline for each of the components. However we'll also look to provide each of the documents needed to conduct each of these tasks.

Also Here's an [FAQ](.\faq.md) with the questions that we've run into most often both internally and for the customer.

## Process documents

- [Create an HSC (SA/Demands)](create_a_hsc.md)
- [Create a Costplan (SA/Demands)](creating_a_costplan.md)
- [Create an Expense Line (TPM/Projects)](creating_expense_line.md)
- [Update an Expense Line to 'Processed'](update_expense_line_to_processed.md)
- [HSC CI lifecycle](hcs_ci_lifecycle.md)
- [General FAQ](faq.md)
- [SA focused FAQ](hsc_guid_sa.md)
- [TPM focused FAQ](hsc_guid_tpm.md)
- Generating a CET for HSC-Dedicated components (PENDING)
- Customer-facing FAQ for questions about HSC charges (PENDING)
