# Proposed Simplified HSC CET

This process is to walk through the simplified **HSC Dedicated - Server and Installation CET** available [here for downloading the latest version](https://gitlab.com/zgates2/bevel-atos-public/-/raw/master/docs/HSC%20-%20Dedicated%20Hardware%20and%20Install%20CET.xlsx).

## TOC

[[_TOC_]]

## Dedicated Server HSC

1. Open the spreadsheet and navigate to the `Hardware Service Charges` tab.

   ![](../media/cet-200.png)

2. Fill out the table using the following as a guide to determine the values on a per line item basis:

   | Field                                         | Options                                                    | Notes                                                        |
   | --------------------------------------------- | ---------------------------------------------------------- | ------------------------------------------------------------ |
   | Model/ Description                            | String                                                     | This should be provided by the vendor and indicate what was ordered. |
   | Link Expense to: CI Name ("Project" if no CI) | String                                                     | This should be generated using the appropriate naming convention with input from the agency. If a CI name is not applicable to the asset (this can be the case for MISC charges such as a spool of cabling), then use the word `"Project"` to fill this field. |
   | Proposal Date                                 | DD/MM/YYYY                                                 | Date that the demand was submitted to the customer.          |
   | Quote Expiration Date                         | DD/MM/YYYY                                                 | Date that the quote expires on. Use '**N/A**' if no expiration date. Please note that this is not **'x' number of days after the `Proposal Date`, but after quote generation**. |
   | Fiscal Year                                   | YYYY                                                       | Fiscal year anticipated for the **Order Date**. Note the fiscal year start on Sept 1st for the following calendar year, for example FY 2021 started on 09/01/2020. |
   | Type of Resource                              | [Server, Appliance, MISC - ...,]                           | This field is required to determine the appropriate installation cost for the resource. |
   | Project Type                                  | [New, Refresh, Expansion]                                  | The project type is important for the computing of the installation costs in some cases. For example for `HSC Dedicated - Servers` the installation charge is not assessed for refreshes, where for `HSC Dedicated - Appliances` they are applicable for refreshes. |
   | Site                                          | [Non-Consolidated, Consolidated - ADC, Consolidated - SDC] | This is the location that device will reside at the end of the project. This is used to establish what is the appropriate tax rate for this quote |
   | Third Party Install Quote                     | Number                                                     | This will override any program installation charges and pass through any installation services being provided by the vendor or a third party. |
   | Pre-tax Quote from Vendor                     | Number                                                     | This will be taken from the total of the vendor generated quote and should be inclusive of software maintenance for 5 years or 60 months. |

3. At the end of this process you should have values formatted as follows:

   ![](../media/cet-201.png)

4. You will then see that within the computer generated section values have been filled out like so:

   ![](../media/cet-202.png)

5. Note that some of the installation costs will be zeroed out if the line item is for a **"MISC"** expense.

---

If you have suggestions or improvements to this document, please feel free to open an [issue](https://gitlab.com/zgates2/bevel-atos-public/-/issues) or send us an [email](mailto:team@bevel.work).
