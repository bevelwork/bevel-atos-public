# Milestone Acceptance Letter Process

[[_TOC_]]

## Summary

Milestone Acceptance and Expense Lines

Critical Payment Milestones are tracked and approved via a Project Task. These approval requests are referred to as the Milestone Acceptance Letter.

## Creating the Project Bench Milestone

When the project progresses to the point to trigger the Payment for the Milestone, the SP Project Manager will request approval for the Payment Milestone by following the steps below in the Milestone Task.

1. Right click on the task and click edit
   ![img](../media/lu2294031bdxq_tmp_ce492b4351439207.png)
1. Modify your view of the task to “Default View”
1. Fill in the Short Description: “Project Number – TPC Project Bench Milestone - #” (if you are doing one payment, the number would be 1, if two payments then 1 would be the first one and 2 would be the second one etc.
   1. Example: PRJ0362272 - TPC Project Bench Milestone - 1
1. Change Time Constraint to “Start on Specific Date
1. Fill in the Description with (Project Task #)-TPC Project Bench Milestone-(Project Name)
   ![img](../media/lu2294031bdxq_tmp_7ed8b3e8f8f54c09.png)
1. Click the “Milestone” and “Key Milestone” box
   1. Payment Milestones will be identified with Key Milestone = “True.”
1. Validate the “Task Requires Approval” box is checked.
1. Validate the Approvers (group) is populated and is the correct CR Approval group for the project
   1. Should be the CR group for the “Bill to Customer” of the project.
1. Add the Planned and Actual Costs
   1. The costs can be found in the Cost Plan with the RU – TPC Project Bench
      ![img](../media/lu2294031bdxq_tmp_9f44c409c604ac37.png)
1. On the Dates tab, fill in the Planned Start and End Date
   1. Note these must be the same as milestones are zero duration.
   2. The Planned Start\End Date should be 10 BD’s from the day you are sending the approval request.
   3. Maybe add a task above to send MAL, make it a 10 BD duration. Then have this milestone below with the task above’s end date.
1. Click Save
   ![img](../media/lu2294031bdxq_tmp_42fc88940fca378d.png)
1. Click “Ready for Approval” on the top right of the project task
   ![img](../media/lu2294031bdxq_tmp_b298008e39bb3f74.png)
1. The state of the project task will change to “Waiting for Approval”
1. The approval can be tracked via the Approvers tab in the Project Task
   1. Note: Must be in Default View in the project task to see the approval
      ![image-20201102093713568](../media/image-20201102093713568.png)

## Processing the Expense Line

When the Milestone Acceptance Letter via the project task has been approved, an expense line can be generated for billing.

1. Open the approved Milestone Task – state on the Milestone is “approved”
   ![img](../media/lu2294031bdxq_tmp_1f3ca0ea2c8788d6.png)
1. Change view to default view
1. Click on the Expense Lines tab
1. Click “New” to create a new Expense Line record
   ![img](../media/lu2294031bdxq_tmp_e352bfa01c7a7ff1.png)
1. Complete the following fields on the expense line form.
   1. Date Incurred – Date MAL sent for approval
   2. Process date – Date MAL was approved
   3. State = Processed
   4. Related Cost (Amount of this Milestone)
   5. Description: Enter Project Name and Milestone Information
   6. Source Table: Project Task
   7. Resource Unit = RU (Project Bench - TPC)
1. Selecting the Milestone task as the Source ID
   1. Click the magnifying glass
   2. Next to Document: field, click magnifying glass again
      ![img](../media/lu2294031bdxq_tmp_f4f588803e505e20.png)
1. Paste in the Project Task Number under the Number field
   ![img](../media/lu2294031bdxq_tmp_4f6fe8d5c5e2cd27.png)
1. Select the Milestone task from the list and click OK
1. Validate the related cost field populated with the actual amount from the project task
1. Click Submit
   ![img](../media/lu2294031bdxq_tmp_5c67968a33fb3670.png)
