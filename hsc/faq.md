# HSC FAQ

[[_TOC_]]

## Service Now Questions

### Q: Where will HSCs be seen within service now?

**A:** All HSCs will be present in the [HSC table available here](https://dirsharedservices.service-now.com/nav_to.do?uri=%2Fu_hsc_list.do). These will also be seen attached to the record they are **associated** with. Thus if the `HSC` has been connected to a configuration item (usually the case), you will see this in the related tables section of the configuration item itself. If the the `HSC` has been related to the project itself, it will show up in the related items section of the project itself.

### Q: Where will the expense lines be seen within service now?

**A:** All `Expense Lines` will be present in the [Expense Line table available here](https://dirsharedservices.service-now.com/nav_to.do?uri=%2Ffm_expense_line_list.do). These will also be seen attached to the record they are **associated** with. Thus if the record has been connected to a configuration item (usually the case), you will see this in the related tables section of the configuration item itself. If the the record has been related to the project itself, it will show up in the related items section of the project itself.

## Billing Questions

### Q: How are installation charges handled?

**A:** A per server/appliance fee RU is assessed for new **and refreshed** devices since 9/1/2020. When a third party is used to perform the installation, those costs are passed directly to the customer.

### Q: What maintenance is included with `HSC` purchases?

**A:** All `HSC` items come with 5 years of maintenance. If non-standard maintenance is required, it can be included in the HSC procurement. It will be listed as a MISC. charge related to the `configuration item`.

### Q: Are `HSC` refreshes handled differently?

**A:** Since 9/1/2020 refreshes and net new deployments are treated the same for HSC - Dedicated expenses.

### Q: When is the agency obligated to pay for HSC components?

**A:** After the project kickoff session, the agency will review each of the HSC components and their latest (or renewed) totals. After this point, the agency is responsible for purchasing HSC components.

### Q: What happens to HSC expenditures if the project is ultimately rejected/cancelled/delayed?

**A:** The TPC will take a `best effort` approach to 're-homing' the equipment or repurposing it for other customers. However, depending on the equipment ordered, this may not be possible. If the equipment cannot be re-homed, the agency will be responsible for HSC costs.

### Q: When will HSC costs show up on my invoice?

**A:** After the equipment has been:

- Received by Atos (Configuration item as `Received`)
- Staged and tested (Configuration item as `Being Assembled`)
- Agency has approved the state of the project via a `Build Validation Letter` or a `Project Acceptance Letter` (whichever is first).

### Q: When would an HSC be charged to an agency in the event of a project cancellation?

**A:** Your project TPM will be in communication with you about this and will be handled on a case-by-case basis.

### Q: Are HSC components subject to PPT and DCS program fees?

**A:** Yes.

### Q: What happens if an HSC quote **expires** between the project acceptance and the project kickoff?

**A:** The TPM assigned to the project will renew the quote with TPC procurement and present the finalized pricing for approval during the `Project Kickoff`.

## Other documentation

### Q: What documentation can I expect for my HSC?

**A:** Here's a list of components created for each HSC:

- An HSC record containing details specific to the HSC.
- Line items that are included in the `Demand`'s `Cost Plan`.
- An expense line with the final cost within the `Project` record.
- A configuration item where appropriate for each asset. Each Configuration item will be related to the HSC details that were created as part of the `Demand` process.
- The `Hardware CET Worksheet` that is attached to the `Demand` or `Project`.
- Copy of the vendor quote attached to the `Demand`.

## MISC HSC Items

### Q: How are HSC 'incidentals' handled? Things like cords, rackmounts, etc.

**A:** These assets will be identical to all the above with the one exception of there may not be a related `configuration item`. Where possible, we will relate these costs to most applicable record, but it is possible for there to be HSC costs with no associated `configuration item`. In this case, the expense line will be associated with the `project` itself instead of a `configuration item` record.

### Additional Questions

1. [Link to Solution Architect FAQ](hsc_guide_sa.md)
1. [Link to Technical Project Manager FAQ](hsc_guide_tpm.md)

If you have other items that you would like to see included in the FAQ or other process docs that would be helpful to you, please send an email to [team@bevel.work]() including the `url` for this document and your suggestion. We'll try hard to keep this up-to-date and relevant.
